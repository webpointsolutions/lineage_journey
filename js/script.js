/*Document Ready*/

jQuery(document).ready(function ($) {

    /*Header Class Scrolling on Page Scroll*/
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if(scroll >= 25){
            $('.site-header').addClass('scrolling');
        } else{
            $('.site-header').removeClass('scrolling');
        }

        /* Parallax Background on Scroll */
        scrollPos = $(this).scrollTop();
        $('.page-banner').css({
            'background-position' : 'center ' + (-scrollPos/4) + 'px'
        });
    });

    /* For All Popup To Close While Clicked on Popup Overlay */
    $('.popup .popup-overlay').click(function () {
        $('.popup').removeClass('active');
    });

    /*For Hamburger Nav*/
    $('#header .nav-bars').click(function () {
        $('.popup-navbar .nav-bars').addClass('open');
        $('.popup-navbar').addClass('active');
        $('.popup-navbar .nav-bars').click(function () {
            $('.popup-navbar .nav-bars').removeClass('open');
            $('.popup-navbar').removeClass('active');
        })
    });

    /*For Secondary Nav Sub Menu*/
    $('.popup-navbar .menu-primary li.hasChild a').click(function () {
        $('.popup-sub-menu-primary .nav-bars').addClass('open');
        $('.popup-sub-menu-primary').addClass('active');
        $('.popup-sub-menu-primary .nav-back').click(function () {
            $('.popup-sub-menu-primary .nav-bars').removeClass('open');
            $('.popup-sub-menu-primary').removeClass('active');
        });
        $('.popup-sub-menu-primary .nav-bars').click(function () {
            $('.popup-nav').removeClass('active');
        })
    });

    /*For Cart*/
    $('.nav-cart').click(function () {
        $('.popup-cart .nav-bars').addClass('open');
        $('.popup-cart').addClass('active');
        $('.popup-cart .nav-bars').click(function () {
            $('.popup-cart .nav-bars').removeClass('open');
            $('.popup-cart').removeClass('active');
        })
    });

    /*For Search Bar*/
    $('#header .nav-search').click(function () {
        $('.popup-search-bar .nav-bars').addClass('open');
        $('.popup-search-bar').addClass('active');
        $('.popup-search-bar .nav-bars').click(function () {
            $('.popup-search-bar .nav-bars').removeClass('open');
            $('.popup-search-bar').removeClass('active');
        })
    });

    /*For Login*/
    $('.btn-login').click(function () {
       $('.popup-login').addClass('active');
       $('.popup-register').removeClass('active');
        $('.popup-reset-password').removeClass('active');
        $('.popup-login .nav-bars').click(function () {
            $('.popup-login').removeClass('active');
        });
    });

    /*For Sign Up*/
    $('.btn-register').click(function () {
        $('.popup-register').addClass('active');
        $('.popup-login').removeClass('active');
        $('.popup-register .nav-bars').click(function () {
            $('.popup-register').removeClass('active');
        });
    });

    /*For Forgot Password*/
    $('.btn-reset').click(function () {
        $('.popup-reset-password').addClass('active');
        $('.popup-login').removeClass('active');
        $('.popup-reset-password .nav-bars').click(function () {
            $('.popup-reset-password').removeClass('active');
        });
    });

    /*For Meet The Team*/
    $('.btn-meet').click(function () {
        $('.popup-team').addClass('active');
        $('.popup-team .nav-bars').click(function () {
            $('.popup-team').removeClass('active');
        });
    });

    /*Hide Select Dropdown List when Clicked Anywhere*/
    $(document).click(function () {
        if(hide) $('.select-box_list').removeClass('showMenu');
        if(hide) $('.select-box-multiple_value').removeClass('activeMenu');
        hide = true;
    });

    /*Custom Select Dropdown*/
    $('.select-box-single_value').click(function () {
        $('.select-box-single_list').toggleClass('showMenu');
        $('.select-box-single_list > li').click(function () {
            $('.select-box-single_value > p').html($(this).html());
            $('.select-box-single_list').removeClass('showMenu');
        });
        hide = false;
    });

    /*Custom Multiple Select Dropdown*/
    $('.select-box-multiple_value').click(function () {
        $(this).toggleClass('activeMenu');
        $('.select-box-multiple_list').toggleClass('showMenu');
        $('.select-box-multiple_list > li').click(function () {
            $('.select-box-multiple_list').removeClass('showMenu');
        });
        hide = false;
    });

    /*Product Additional Info Tab Section*/
    $('ul.tab-head li:first-child').addClass('active');
    $('.tab-body .tab-content:first-child').addClass('active');
    $('.tab-head .tab-item').click(function (event) {
        event.preventDefault();
        $(this).addClass('active');
        $(this).siblings().removeClass('active');

        var tab = $(this).attr('for');
        $(tab).addClass('active');
        $('.tab-content').removeClass('d-none');
        $('.tab-content').not(tab).addClass('d-none');
        $('.tab-content').not(tab).removeClass('active');
        $(tab).show();
    });

    /*For FAQ Slide Tabs*/
    $('.faq-title').click(function (event) {
        event.preventDefault();
        $(this).parent().toggleClass('active');
        $(this).toggleClass('current');
        $(this).siblings().slideToggle();
        $('.faq-list').not($(this).parent()).removeClass('active');
        $('.faq-title').not($(this)).removeClass('current');
        $('.faq-content').not($(this).siblings()).slideUp();
    })
});


